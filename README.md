# Stylus Styles

A collection of Stylus Styles for Colorizing the Web! Give your eyes a break, Give your brain a break 🤓